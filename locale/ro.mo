��    S      �  q   L                          %     )     2     8     =  :   B     }     �     �     �     �  $   �  :         I     j     x     �     �  	   �  
   �     �     �     �  B   �  @   �     ;	  	   L	     V	     _	     e	  #   n	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	     �	  !   
     -
  g   M
  )   �
     �
     �
  	   �
  
                  !     .     <  	   C     M     Y     l     z     �     �     �     �     �     �     �     �     �     �     �  +   �     '     -     5     D     V     _     s     |  ;  �     �     �     �     �     �     �            5        A     S     n     �     �  '   �  )   �     �               '     .     7  
   C  	   N  	   X     b  9   i  :   �     �     �     �       	     +        >     Z     `     q     }     �     �     �     �     �     �     �  A   �     >     ]     v     }     �     �     �     �     �     �     �     �     �               '     =     B     H  
   N  
   Y  
   d     o  
   {     �     �  +   �  	   �     �     �     �     	          %     .        -                              8             +          (   	   $   1   2      "   I   B   C   O   H   *          5      E   7   J   9   S       4                  %          A      @   G         0   
   >      R       K          L   <   ;      '   ,       Q              =                        .      M   F              :   6   &          #           /       )              D   ?               P       N   !   3    : Abort Game? About Add Add Unit Apply Back Both Can't load vocabulary list!
The data structure is invalid. Code Snippets & Addons Control players with arrow keys Create without Saving Default Directory Default File Do you want to quit the application? Do you want to restart the game to adapt the new language? Do you want to start a new game? Documentation English Error! Fonts G O A L ! Game Setup General German Goals If you create a new vocabulary list all changes will be discarded! If you open a new vocabulary list all changes will be discarded! Keyboard Control Kick-off! Language Latin Licenses Load a file or create a first unit. Made with Godot Engine 4 Name New File New Game Next No File Loaded None Number of Players Open File Open without Saving Please choose one unit at least.
 Please load a vocabulary list.
 Please load a vocabulary list.
Pleas choose one unit at least.
Please type in two different team names. Please type in two different team names.
 Programming & Design Quit Quit Game Quit Game? Remove Unit Rename Restart Game Restart Game? Revert Save File Save a File Select a Directory Select a File Settings Show Translation Sounds Source Source Language Target Target Language Team 1 Team 2 Teams Textures & Icons Timer Length Type in some vocables for the current unit. Units Version Vocable Editor Vocable Selection Vocables Vocabulary Football Warning! © 2024 Roma Aeterna Project-Id-Version: Vocabulary Football
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: ro
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n==0 || (n!=1 && n%100>=1 && n%100<=19) ? 1 : 2);
X-Generator: Poedit 3.4
 : Interruptio ludi? De ludo Adde Adde lectionem Fac Redi Ambo Tabularium aperi non potui! Forma datorum irrita est. Auxilia & Adducta Guberna lusorem claviatura Crea sine conditione Ordinator vulgaris Index vulgaris Visne finem huius applicationis facere? Visne ludum renovare ad linguam mutandam? Visne finem huius ludi facere? Auxilium Anglica Error! Litterae P O R T A ! Leges ludi Generalia Germanica Portae Si tabularium novum creabis, omnes mutationes spernentur! Si tabularium novum aperibis, omnes mutationes spernentur! Gubernatio claviaturae Age! Lingua Latina Licentiae Aperi tabularium aut crea lectionem primam. "Godot Engine IV" confectum Nomen Tabularium novum Ludus novus Perge Tabularium inane Nullum Numerus pedilusorum Aperi tabularium Aperi sine conditione Delige lectionem!
 Aperi tabularium!
 Aperi tabularium.
Delige lectionem.
Inscribe duo nomina disparia. Inscribe duo nomina disparia!
 Programmatio & Ornamenta Exitus Exitus ludi Finis ludi? Dele lectionem Transnomina Renovatio ludi Renovatio ludi? Reverte Conde tabularium Conde tabularium Delige ordinatorem Delige tabularium Optiones Ostende translationem Soni Origo Origo Destinatum Destinatum Societas I Societas II Societates Texturae & Imagines Spatium temporis Inscribe prima vocabula lectionis recentis. Lectiones versio Editor verborum Electio verborum Vocabula Pediludium verbale Monitio! © MMXXIV Roma Aeterna 