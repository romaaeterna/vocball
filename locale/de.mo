��    S      �  q   L                          %     )     2     8     =  :   B     }     �     �     �     �  $   �  :         I     j     x     �     �  	   �  
   �     �     �     �  B   �  @   �     ;	  	   L	     V	     _	     e	  #   n	     �	     �	     �	     �	     �	     �	     �	     �	  	   �	     �	  !   
     -
  g   M
  )   �
     �
     �
  	   �
  
                  !     .     <  	   C     M     Y     l     z     �     �     �     �     �     �     �     �     �     �     �  +   �     '     -     5     D     V     _     s     |  �   �     n     p     �     �     �     �     �     �  F   �                >     Z     j  &   y  @   �  &   �                    '     4     @     N     Z     b  M   g  N   �                     (     /  +   8  #   d     �  
   �     �     �     �     �     �     �     �  *        ,  t   L  ,   �     �                    +  
   =     H     Z     l     z     �     �     �     �     �     �     �     �                          '     -     >  ,   O  	   |     �     �     �     �     �     �     �        -                              8             +          (   	   $   1   2      "   I   B   C   O   H   *          5      E   7   J   9   S       4                  %          A      @   G         0   
   >      R       K          L   <   ;      '   ,       Q              =                        .      M   F              :   6   &          #           /       )              D   ?               P       N   !   3    : Abort Game? About Add Add Unit Apply Back Both Can't load vocabulary list!
The data structure is invalid. Code Snippets & Addons Control players with arrow keys Create without Saving Default Directory Default File Do you want to quit the application? Do you want to restart the game to adapt the new language? Do you want to start a new game? Documentation English Error! Fonts G O A L ! Game Setup General German Goals If you create a new vocabulary list all changes will be discarded! If you open a new vocabulary list all changes will be discarded! Keyboard Control Kick-off! Language Latin Licenses Load a file or create a first unit. Made with Godot Engine 4 Name New File New Game Next No File Loaded None Number of Players Open File Open without Saving Please choose one unit at least.
 Please load a vocabulary list.
 Please load a vocabulary list.
Pleas choose one unit at least.
Please type in two different team names. Please type in two different team names.
 Programming & Design Quit Quit Game Quit Game? Remove Unit Rename Restart Game Restart Game? Revert Save File Save a File Select a Directory Select a File Settings Show Translation Sounds Source Source Language Target Target Language Team 1 Team 2 Teams Textures & Icons Timer Length Type in some vocables for the current unit. Units Version Vocable Editor Vocable Selection Vocables Vocabulary Football Warning! © 2024 Roma Aeterna Project-Id-Version: Vocabulary Football
PO-Revision-Date: 
Last-Translator: 
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4
 : Spiel abbrechen? Über Hinzufügen Lektion hinzufügen Anwenden Zurück Beide Vokabelliste kann nicht geladen werden! Das Datenformat ist ungültig. Code-Schnipsel & Addons Spieler mit der Tastatur steuern Erstellen ohne zu speichern Standard-Ordner Standard-Datei Soll die Anwendung geschlossen werden? Soll das Spiel neu gestartet werden, um die Sprache zu wechseln? Soll ein neues Spiel gestartet werden? Dokumentation Englisch Fehler! Schriftarten T O O O R ! Spieloptionen Allgemeines Deutsch Tore Wenn eine neue Vokabelliste erstellt wird, werden alle Änderungen verworfen! Wenn eine neue Vokabelliste geöffnet wird, werden alle Änderungen verworfen! Tastatur-Steuerung Anstoß! Sprache Latein Lizenzen Datei öffnen oder erste Lektion erstellen. Programmiert mit der Godot Engine 4 Name Neue Datei Neues Spiel Weiter Keine Datei geladen Kein/e Anzahl der Spieler Datei öffnen Öffnen ohne zu speichern Bitte mindestens eine Lektion auswählen.
 Bitte eine Vokabelliste laden.
 Bitte eine Vokabelliste laden.
Bitte mindestens eine Lektion auswählen.
Bitte zwei verschiedene Teamnamen eingeben. Bitte zwei verschiedene Teamnamen eingeben.
 Programmierung & Design Beenden Spiel beenden Spiel beenden? Lektion entfernen Umbenennen Spiel neu starten Spiel neustarten? Zurücksetzen Datei speichern Datei speichern Ordner auswählen Datei auswählen Einstellungen Übersetzung anzeigen Soundeffekte Herkunft Herkunftssprache Ziel Zielsprache Team 1 Team 2 Teams Texturen & Icons Dauer des Timers Vokabeln für die aktuelle Lektion eingeben. Lektionen Version Vokabeleditor Vokabelauswahl Vokabeln Vokabelfußball Warnung! © 2024 Roma Aeterna 