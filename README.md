# Vocabulary Football (VocBall)

## Introduction

VocBall is a simple application to play vocabulary football in class.

You can download the game from the [Itch.io website](https://romaaeterna.itch.io/vocball).

See the [online documentation](https://vocball.readthedocs.io/en/latest/) for instructions on how to play.

## Source code & License

The source code of Vocabulary Football is hosted on [GitLab](https://gitlab.com/romaaeterna/vocball).

The game is released under the [GNU General Public License (GPL) version 3](https://www.gnu.org/licenses/gpl-3.0).
See the file [LICENSE.txt](LICENSE.txt) for more information.

## Special thanks to

- the developers of the [Godot Engine](https://godotengine.org/)
- the Latin students of the [Vicco-von-Bülow-Gymnasium Falkensee](http://www.vicco-von-buelow-gymnasium-falkensee.de/) (for being enthusiastic beta testers)
