class_name MessageDialog
extends Window


@onready var vbox_content = $MarginDialog/TextureDialog/MarginContent/VBoxContent
@onready var vbox_text = vbox_content.get_node("VBoxText")
@onready var label_title = vbox_text.get_node("LabelTitle")
@onready var label_main = vbox_text.get_node("LabelMain")
@onready var vbox_buttons = vbox_content.get_node("VBoxButtons")
@onready var button_back = vbox_buttons.get_node("ButtonBack")
@onready var label_back = button_back.get_node("LabelBack")
@onready var button_close = vbox_buttons.get_node("ButtonClose")
@onready var label_close = button_close.get_node("LabelClose")
