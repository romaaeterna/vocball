class_name Setup
extends MarginContainer


var message_dialog = preload("res://scenes/message_dialog.tscn").instantiate()

# teams
var argentina = {"bg": Color.LIGHT_SKY_BLUE, "fg": Color.BLACK, "name": "argentina",
    "texture": load("res://assets/textures/player_argentina_happy.svg")}
var australia = {"bg": Color.YELLOW, "fg": Color.DARK_GREEN, "name": "australia",
    "texture": load("res://assets/textures/player_australia_happy.svg")}
var brazil = {"bg": Color.GOLD, "fg": Color.MEDIUM_BLUE, "name": "brazil",
    "texture": load("res://assets/textures/player_brazil_happy.svg")}
var england = {"bg": Color.WHITE, "fg": Color.DARK_BLUE, "name": "england",
    "texture": load("res://assets/textures/player_england_happy.svg")}
var france = {"bg": Color.DARK_BLUE, "fg": Color.WHITE, "name": "france",
    "texture": load("res://assets/textures/player_france_happy.svg")}
var germany = {"bg": Color.WHITE, "fg": Color.BLACK, "name": "germany",
    "texture": load("res://assets/textures/player_germany_happy.svg")}
var italy = {"bg": Color.DODGER_BLUE, "fg": Color.WHITE, "name": "italy",
    "texture": load("res://assets/textures/player_italy_happy.svg")}
var japan = {"bg": Color.MEDIUM_BLUE, "fg": Color.WHITE, "name": "japan",
    "texture": load("res://assets/textures/player_japan_happy.svg")}
var korea = {"bg": Color.DARK_RED, "fg": Color.WHITE, "name": "korea",
    "texture": load("res://assets/textures/player_korea_happy.svg")}
var mexico = {"bg": Color.WEB_GREEN, "fg": Color.WHITE, "name": "mexico",
    "texture": load("res://assets/textures/player_mexico_happy.svg")}
var netherlands = {"bg": Color.ORANGE, "fg": Color.BLACK, "name": "netherlands",
    "texture": load("res://assets/textures/player_netherlands_happy.svg")}
var portugal = {"bg": Color.RED, "fg": Color.WHITE, "name": "portugal",
    "texture": load("res://assets/textures/player_portugal_happy.svg")}
var senegal = {"bg": Color.DARK_GREEN, "fg": Color.WHITE, "name": "senegal",
    "texture": load("res://assets/textures/player_senegal_happy.svg")}
var spain = {"bg": Color.CRIMSON, "fg": Color.YELLOW, "name": "spain",
    "texture": load("res://assets/textures/player_spain_happy.svg")}
var turkey = {"bg": Color.WHITE_SMOKE, "fg": Color.DARK_RED, "name": "turkey",
    "texture": load("res://assets/textures/player_turkey_happy.svg")}
var uruguay = {"bg": Color.DEEP_SKY_BLUE, "fg": Color.WHITE, "name": "uruguay",
    "texture": load("res://assets/textures/player_uruguay_happy.svg")}

var countries = [argentina, australia, brazil, england, france, germany, italy,
    japan, korea, mexico, netherlands, portugal, senegal, spain, turkey, uruguay]
var current_textures = [brazil.texture, germany.texture]
var setup_finished = false
var textures = [argentina.texture, australia.texture, brazil.texture, england.texture,
    france.texture, germany.texture, italy.texture, japan.texture, korea.texture,
    mexico.texture, netherlands.texture, portugal.texture, senegal.texture,
    spain.texture, turkey.texture, uruguay.texture]


@onready var hbox_top = $VBoxSetup/HBoxTop
@onready var button_apply = hbox_top.get_node("ButtonApply")
@onready var hbox_tabs = hbox_top.get_node("CenterTabs/HBoxTabs")
@onready var button_general = hbox_tabs.get_node("ButtonGeneral")
@onready var button_teams = hbox_tabs.get_node("ButtonTeams")

@onready var hbox_general = $VBoxSetup/TabSetup/CenterGeneral/HBoxGeneral
@onready var vbox_file = hbox_general.get_node("VBoxFile")
@onready var label_file = vbox_file.get_node("HBoxFile/LabelFile")
@onready var button_file = vbox_file.get_node("HBoxFile/ButtonFile")
@onready var flow_units = vbox_file.get_node("PanelUnits/ScrollUnits/FlowUnits")
@onready var vbox_various = hbox_general.get_node("VBoxVarious")
@onready var vbox_vocable_selection = vbox_various.get_node("VBoxVocableSelection")
@onready var hbox_vocable_selection = vbox_vocable_selection.get_node("HBoxVocableSelection")
@onready var button_source = hbox_vocable_selection.get_node("ButtonSource")
@onready var button_target = hbox_vocable_selection.get_node("ButtonTarget")
@onready var button_both = hbox_vocable_selection.get_node("ButtonBoth")
@onready var vbox_players = vbox_various.get_node("VBoxPlayers")
@onready var slider_player = vbox_players.get_node("HBoxPlayers/SliderPlayer")
@onready var label_player = vbox_players.get_node("HBoxPlayers/LabelPlayer")

@onready var tab_setup = $VBoxSetup/TabSetup
@onready var hbox_teams = tab_setup.get_node("CenterTeams/HBoxTeams")
@onready var hbox_team1 = hbox_teams.get_node("HBoxTeam1")
@onready var grid_team1 = hbox_team1.get_node("VBoxTeam1/GridTeam1")
@onready var line_name1 = grid_team1.get_node("LineName1")
@onready var slider_goal1 = grid_team1.get_node("HBoxGoal1/SliderGoal1")
@onready var label_goal1 = grid_team1.get_node("HBoxGoal1/LabelGoal1")
@onready var texture_shirt1 = hbox_team1.get_node("PanelShirt1/MarginShirt1/HBoxShirt1/TextureShirt1")
@onready var hbox_team2 = hbox_teams.get_node("HBoxTeam2")
@onready var grid_team2 = hbox_team2.get_node("VBoxTeam2/GridTeam2")
@onready var line_name2 = grid_team2.get_node("LineName2")
@onready var slider_goal2 = grid_team2.get_node("HBoxGoal2/SliderGoal2")
@onready var label_goal2 = grid_team2.get_node("HBoxGoal2/LabelGoal2")
@onready var texture_shirt2 = hbox_team2.get_node("PanelShirt2/MarginShirt2/HBoxShirt2/TextureShirt2")

@onready var animation_setup = $AnimationSetup
@onready var animation_game = get_tree().get_root().get_node("Game/CameraScoreboard/AnimationCamera")


func _on_ready():
    add_child(message_dialog)
    message_dialog.button_back.pressed.connect(message_dialog_back_pressed)
    
    if global.last_vocable_file:
        _on_native_file_dialog_file_selected(null, [global.last_vocable_file], null)
    elif global.default_file:
        _on_native_file_dialog_file_selected(null, [global.default_file], null)
    if global.vocable_selection == "source":
        button_source.button_pressed = true
    elif global.vocable_selection == "target":
        button_target.button_pressed = true
    elif global.vocable_selection == "both":
        button_both.button_pressed = true


func _on_button_apply_pressed():
    setup_finished = true
    setup_general()
    setup_team1()
    setup_team2()
    animation_game.play("zoom_out")


func _on_button_close_pressed():
    animation_game.play("zoom_out")


func _on_button_general_pressed():
    tab_setup.current_tab = 0
    button_general.button_pressed = true
    button_teams.button_pressed = false


func _on_button_teams_pressed():
    tab_setup.current_tab = 1
    button_general.button_pressed = false
    button_teams.button_pressed = true


func _on_button_file_pressed():
    var current_dir = ""
    if global.last_vocable_file and DirAccess.open(global.last_vocable_file):
        current_dir = global.last_vocable_file.get_base_dir()
    elif global.default_dir and DirAccess.open(global.default_dir):
        current_dir = global.default_dir
    var title = tr("Select a File")
    var mode = DisplayServer.FILE_DIALOG_MODE_OPEN_FILE
    var csv_filter = PackedStringArray(["*.csv ; CSV Files"])
    DisplayServer.file_dialog_show(title, current_dir, "", false, mode, csv_filter,
        _on_native_file_dialog_file_selected)


func _on_native_file_dialog_file_selected(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    
    var csv_content = load_file(selected_paths[0])
    get_vocabulary(csv_content)
    if not global.vocables:
        for child in flow_units.get_children():
            flow_units.remove_child(child)
        global.last_vocable_file = ""
        label_file.text = tr("No File Loaded")
        return show_message_dialog()
    global.last_vocable_file = selected_paths[0]
    label_file.text = selected_paths[0].get_file()
    add_unit_buttons()
    check_data_complete()


func _on_unit_button_toggled(_toggled):
    check_data_complete()


func _on_slider_player_value_changed(value):
    label_player.text = str(value)


func _on_line_name_text_changed(_new_name):
    check_data_complete()


func _on_slider_goal_1_value_changed(value):
    label_goal1.text = str(value)


func _on_slider_goal_2_value_changed(value):
    label_goal2.text = str(value)


func _on_button_left_pressed(index):
    if index == 0:
        get_previous_texture(index, texture_shirt1)
    if index == 1:
        get_previous_texture(index, texture_shirt2)


func _on_button_right_pressed(index):
    if index == 0:
        get_next_texture(index, texture_shirt1)
    if index == 1:
        get_next_texture(index, texture_shirt2)


func load_file(file_path):
    var file = FileAccess.open(file_path, FileAccess.READ)
    return file.get_as_text()


func get_vocabulary(file_content):
    var csv_array = file_content.split("\n")
    global.unit_names = []
    global.vocables = []
    
    for i in range(csv_array.size()):
        var line_array = csv_array[i].replace(";",",").replace("\t",",").split(",")
        if line_array.size() == 3:
            global.vocables.append(global.vocable.new(line_array[0], line_array[1], line_array[2]))
            if not line_array[0] in global.unit_names:
                global.unit_names.append(line_array[0])


func add_unit_buttons():
    for child in flow_units.get_children():
        flow_units.remove_child(child)
    global.unit_names.sort_custom(func(a, b): return a.naturalnocasecmp_to(b) < 0)
    
    for unit_name in global.unit_names:
        var button = Button.new()
        button.add_theme_stylebox_override("hover", load("res://tres/unit_button_hovered.tres"))
        button.add_theme_stylebox_override("normal", load("res://tres/unit_button_normal.tres"))
        button.add_theme_stylebox_override("pressed", load("res://tres/unit_button_pressed.tres"))
        button.add_theme_stylebox_override("focus", StyleBoxEmpty.new())
        button.custom_minimum_size = Vector2(39, 0)
        button.text = str(unit_name)
        button.toggle_mode = true
        button.toggled.connect(_on_unit_button_toggled)
        flow_units.add_child(button)


func get_previous_texture(index, texture_to_change):
    var textures_unused = []
    for texture in textures:
        if not texture in current_textures:
            textures_unused.append(texture)
    textures_unused.reverse()
    
    var index_old = textures.find(current_textures[index])
    for texture in textures_unused:
        var index_new = textures.find(texture)
        if index_new < index_old:
            current_textures[index] = textures[index_new]
            texture_to_change.texture = textures[index_new]
            return
    current_textures[index] = textures_unused[0]
    texture_to_change.texture = textures_unused[0]


func get_next_texture(index, texture_to_change):
    var textures_unused = []
    for texture in textures:
        if not texture in current_textures:
            textures_unused.append(texture)
    
    var index_old = textures.find(current_textures[index])
    for texture in textures_unused:
        var index_new = textures.find(texture)
        if index_new > index_old:
            current_textures[index] = textures[index_new]
            texture_to_change.texture = textures[index_new]
            return
    current_textures[index] = textures_unused[0]
    texture_to_change.texture = textures_unused[0]


func check_data_complete():
    var complete = true
    var error_text = ""
    var unit_selected = false
    
    if flow_units.get_children() == []:
        complete = false
        error_text = tr("Please load a vocabulary list.\n")
    
    for child in flow_units.get_children():
        if child.button_pressed:
            unit_selected = true
            break
    if not unit_selected:
        complete = false
        error_text += tr("Please choose one unit at least.\n")
    
    if line_name1.text == "" or line_name2.text == "" or line_name1.text == line_name2.text:
        complete = false
        error_text += tr("Please type in two different team names.\n")
    
    button_apply.disabled = !complete
    button_apply.tooltip_text = error_text


func setup_general():
    var selected_units = []
    global.unit_vocables = []
    
    for child in flow_units.get_children():
        if child.button_pressed:
            selected_units.append(child.text)
    for vocable in global.vocables:
        if vocable.unit in selected_units:
            global.unit_vocables.append(vocable)
    
    if button_source.button_pressed:
        global.vocable_selection = "source"
    elif button_target.button_pressed:
        global.vocable_selection = "target"
    elif button_both.button_pressed:
        global.vocable_selection = "both"
    global.player_number = slider_player.value


func setup_team1():
    global.team1.name = line_name1.text
    global.team1.score = slider_goal1.value
    global.team1.texture = texture_shirt1.texture
    global.team1.country = texture_shirt1.texture.resource_path.split("_")[1]
    for country in countries:
        if global.team1.country == country.name:
            global.team1.color_bg = country.bg
            global.team1.color_fg = country.fg
            break


func setup_team2():
    global.team2.name = line_name2.text
    global.team2.score = slider_goal2.value
    global.team2.texture = texture_shirt2.texture
    global.team2.country = texture_shirt2.texture.resource_path.split("_")[1]
    for country in countries:
        if global.team2.country == country.name:
            global.team2.color_bg = country.bg
            global.team2.color_fg = country.fg
            break


func show_message_dialog():
    var text = tr("Can't load vocabulary list!\nThe data structure is invalid.")
    message_dialog.label_title.text = tr("Error!")
    message_dialog.label_main.text = text
    message_dialog.button_close.hide()
    message_dialog.popup_centered()


func message_dialog_back_pressed():
    message_dialog.hide()
