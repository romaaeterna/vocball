class_name Editor
extends Panel


var message_dialog = preload("res://scenes/message_dialog.tscn").instantiate()

var button_black = load("res://assets/ui/button_square_black.svg")
var button_black_hovered = load("res://assets/ui/button_square_black_hovered.svg")
var button_green = load("res://assets/ui/button_square_green.svg")
var button_green_hovered = load("res://assets/ui/button_square_green_hovered.svg")

var csv_filter = PackedStringArray(["*.csv ; CSV Files"])
var change_unit_name = false
var current_file_path = ""
var current_unit_index = 0
var current_unit_name = ""
var unit_names = []
var unit_vocables = []
var vocables = []
var vocables_on_load = []


@onready var vbox_file = $MarginEditor/VBoxEditor/PanelFile/MarginFile/VBoxFile
@onready var hbox_file = vbox_file.get_node("HBoxFile")
@onready var button_file_new = hbox_file.get_node("ButtonFileNew")
@onready var button_file_open = hbox_file.get_node("ButtonFileOpen")
@onready var button_file_save = hbox_file.get_node("ButtonFileSave")
@onready var label_file = hbox_file.get_node("LabelFile")
@onready var hbox_units = vbox_file.get_node("HBoxUnits")
@onready var button_unit_add = hbox_units.get_node("ButtonUnitAdd")
@onready var button_unit_remove = hbox_units.get_node("ButtonUnitRemove")
@onready var flow_units = vbox_file.get_node("PanelUnits/ScrollUnits/FlowUnits")
@onready var vbox_vocables = $MarginEditor/VBoxEditor/PanelVocables/MarginVocables/VBoxVocables
@onready var tree_vocables = vbox_vocables.get_node("TreeVocables")
@onready var hbox_warning = vbox_vocables.get_node("HBoxWarning")
@onready var label_warning = hbox_warning.get_node("LabelWarning")

@onready var popup_unit_add = $PopupUnitAdd
@onready var vbox_unit_add = popup_unit_add.get_node("MarginUnitAdd/VBoxUnitAdd")
@onready var line_unit = vbox_unit_add.get_node("LineUnit")
@onready var button_unit_popup = vbox_unit_add.get_node("ButtonUnitPopup")
@onready var label_unit_popup = button_unit_popup.get_node("LabelUnitPopup")

@onready var animation_editor = $AnimationEditor
@onready var game = get_tree().get_root().get_node("Game")
@onready var texture_blur = game.get_node("TextureBlur")


func _on_ready():
    add_child(message_dialog)
    message_dialog.button_back.pressed.connect(message_dialog_back_pressed)
    message_dialog.button_close.pressed.connect(message_dialog_close_pressed)
    tree_vocables.set_column_title(0, tr("Source Language"))
    tree_vocables.set_column_title(1, tr("Target Language"))


func _on_animation_editor_animation_finished(animation_name):
    if animation_name == "slide_in":
        texture_blur.show()
    if animation_name == "slide_out":
        if global.keyboard_control:
            game.set_process(true)
        texture_blur.hide()


func _on_button_close_pressed():
    animation_editor.play("slide_out")


func _on_button_file_new_pressed():
    modify_unit_vocables()
    if vocables_on_load.hash() != vocables.hash():
        return show_message_dialog("new_file")
    clear_data()


func _on_button_file_open_pressed():
    modify_unit_vocables()
    if vocables_on_load.hash() != vocables.hash():
        return show_message_dialog("load_file")
    var title = tr("Select a File")
    var mode = DisplayServer.FILE_DIALOG_MODE_OPEN_FILE
    DisplayServer.file_dialog_show(title, global.default_dir, "", false, mode, csv_filter,
        _on_native_file_dialog_file_selected)


func _on_button_file_save_pressed():
    modify_unit_vocables()
    if label_file.text == tr("No File Loaded"):
        var title = tr("Save a File")
        var mode = DisplayServer.FILE_DIALOG_MODE_SAVE_FILE
        DisplayServer.file_dialog_show(title, global.default_dir, "", false, mode, csv_filter,
            _on_native_file_dialog_save_file)
        return
    var file_content = get_vocabulary_to_save()
    save_file(current_file_path, file_content)


func _on_native_file_dialog_file_selected(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    
    var csv_content = load_file(selected_paths[0])
    get_vocabulary_to_load(csv_content)
    if not vocables:
        _on_button_file_new_pressed()
        return show_message_dialog("error")
    current_file_path = selected_paths[0]
    label_file.text = selected_paths[0].get_file()
    add_unit_buttons()
    check_vocable_data()


func _on_native_file_dialog_save_file(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    
    var path = selected_paths[0]
    path = path + ".csv" if not path.to_lower().ends_with(".csv") else path
    current_file_path = path
    label_file.text = path.get_file()
    
    var file_content = get_vocabulary_to_save()
    save_file(path, file_content)


func _on_unit_button_toggled(toggled):
    if not toggled:
        return
    modify_unit_vocables()
    tree_vocables.clear()
    tree_vocables.create_item()
    unit_vocables = []
    
    for child in flow_units.get_children():
        if child.button_pressed:
            current_unit_index = child.get_index()
            current_unit_name = child.text
            for vocable in vocables:
                if vocable.unit == child.text:
                    unit_vocables.append(vocable)
                    add_vocable_to_tree(vocable, false)
            add_vocable_to_tree(global.vocable.new(current_unit_name, "", ""), true)
            check_vocable_data()
            disable_button(button_unit_remove, false)
            return


func _on_unit_button_gui_input(event):
    if event is InputEventMouseButton:
        if event.double_click or (event.pressed and event.button_index == 2):
            change_unit_name = true
            label_unit_popup.text = tr("Rename")
            line_unit.text = current_unit_name
            line_unit.grab_focus()
            line_unit.select_all()
            popup_unit_add.popup(Rect2i(1632, 238, 272, 151))


func _on_button_unit_add_pressed():
    line_unit.grab_focus()
    popup_unit_add.popup(Rect2i(1632, 238, 272, 151))


func _on_button_unit_remove_pressed():
    for child in flow_units.get_children():
        if child.button_pressed:
            var unit_name = child.text
            remove_unit_vocables(unit_name)
            flow_units.remove_child(child)
            disable_button(button_unit_remove, true)
            select_new_unit()
            check_vocable_data()
            return


func _on_popup_unit_add_popup_hide():
    change_unit_name = false
    label_unit_popup.text = tr("Add")
    line_unit.text = ""


func _on_line_unit_text_changed(new_text):
    if new_text and not new_text in unit_names:
        disable_button(button_unit_popup, false)
    else:
        disable_button(button_unit_popup, true)


func _on_line_unit_gui_input(event):
    if button_unit_popup.disabled:
        return
    if event is InputEventKey and event.pressed:
        if event.keycode in [KEY_ENTER, KEY_KP_ENTER]:
            _on_button_unit_add_popup_pressed()


func _on_button_unit_add_popup_pressed():
    modify_unit_vocables()
    if change_unit_name:
        return rename_unit()
    
    create_button(line_unit.text)
    current_unit_index = flow_units.get_child_count()
    current_unit_name = line_unit.text
    unit_names.append(current_unit_name)
    
    disable_button(button_unit_popup, true)
    flow_units.get_children()[-1].button_pressed = true
    check_vocable_data()
    popup_unit_add.hide()


func _on_tree_vocables_item_edited():
    var item = tree_vocables.get_edited()
    var vocable = item.get_metadata(0)
    
    if item.get_next():
        if not item.get_text(0) and not item.get_text(1):
            unit_vocables.erase(vocable)
            vocables.erase(vocable)
            tree_vocables.get_root().remove_child(item)
        else:
            vocable.source = item.get_text(0)
            vocable.target = item.get_text(1)
    else:
        vocable.source = item.get_text(0)
        vocable.target = item.get_text(1)
        if item.get_text(0) and item.get_text(1):
            add_vocable_to_tree(global.vocable.new(current_unit_name, "", ""), true)
            tree_vocables.scroll_to_item(item.get_next())
    check_vocable_data()


func clear_data():
    unit_names = []
    unit_vocables = []
    vocables = []
    vocables_on_load = []
    
    label_file.text = tr("No File Loaded")
    line_unit.text = ""
    for child in flow_units.get_children():
        flow_units.remove_child(child)
    tree_vocables.clear()
    check_vocable_data()
    disable_button(button_unit_popup, true)
    disable_button(button_unit_remove, true)


func load_file(file_path):
    var file = FileAccess.open(file_path, FileAccess.READ)
    return file.get_as_text()


func save_file(file_path, file_content):
    var file = FileAccess.open(file_path, FileAccess.WRITE)
    file.store_string(file_content)


func get_vocabulary_to_load(file_content):
    var csv_array = file_content.split("\n")
    unit_names = []
    vocables = []
    
    for i in range(csv_array.size()):
        var line_array = csv_array[i].replace(";",",").replace("\t",",").split(",")
        if line_array.size() == 3:
            vocables.append(global.vocable.new(line_array[0], line_array[1], line_array[2]))
            if not line_array[0] in unit_names:
                unit_names.append(line_array[0])
    vocables_on_load = vocables.duplicate(true)


func get_vocabulary_to_save():
    var file_content = ""
    for vocable in vocables:
        file_content += vocable.unit + "," + vocable.source + "," + vocable.target + "\n"
    return file_content


func add_unit_buttons():
    for child in flow_units.get_children():
        flow_units.remove_child(child)
    unit_names.sort_custom(func(a, b): return a.naturalnocasecmp_to(b) < 0)
    
    for unit_name in unit_names:
        create_button(unit_name)
    flow_units.get_children()[-1].button_pressed = true


func create_button(unit_name):
    var button = Button.new()
    button.add_theme_stylebox_override("hover", load("res://tres/unit_button_hovered.tres"))
    button.add_theme_stylebox_override("normal", load("res://tres/unit_button_normal.tres"))
    button.add_theme_stylebox_override("pressed", load("res://tres/unit_button_pressed.tres"))
    button.add_theme_stylebox_override("focus", StyleBoxEmpty.new())
    button.button_group = load("res://tres/button_group_units.tres")
    button.custom_minimum_size = Vector2(86, 0)
    button.text = str(unit_name)
    button.toggle_mode = true
    button.gui_input.connect(_on_unit_button_gui_input)
    button.toggled.connect(_on_unit_button_toggled)
    flow_units.add_child(button)


func select_new_unit():
    if not flow_units.get_child_count():
        tree_vocables.clear()
        return
    if current_unit_index == flow_units.get_child_count():
        current_unit_index -= 1
    flow_units.get_children()[current_unit_index].button_pressed = true


func rename_unit():
    var old_name = current_unit_name
    var new_name = line_unit.text
    flow_units.get_child(current_unit_index).text = new_name
    for vocable in unit_vocables:
        if vocable.unit == old_name:
            vocable.unit = new_name
    current_unit_name = new_name
    unit_names[unit_names.find(old_name)] = new_name
    
    check_vocable_data()
    disable_button(button_unit_popup, true)
    popup_unit_add.hide()


func add_vocable_to_tree(vocable, new):
    var vocable_to_add = tree_vocables.create_item(tree_vocables.get_root())
    vocable_to_add.set_cell_mode(0, 0)
    vocable_to_add.set_cell_mode(1, 0)
    vocable_to_add.set_editable(0, true)
    vocable_to_add.set_editable(1, true)
    vocable_to_add.set_metadata(0, vocable)
    vocable_to_add.set_text(0, vocable.source)
    vocable_to_add.set_text(1, vocable.target)
    vocable_to_add.set_text_overrun_behavior(0, 3)
    vocable_to_add.set_text_overrun_behavior(1, 3)
    if new:
        unit_vocables.append(vocable)
        vocables.append(vocable)


func modify_unit_vocables():
    if not current_unit_name or not tree_vocables.get_root():
        return
    for item in tree_vocables.get_root().get_children():
        if not item.get_text(0) or not item.get_text(1):
            vocables.erase(item.get_metadata(0))


func remove_unit_vocables(unit_name):
    for i in range(vocables.size()-1, -1, -1):
        if vocables[i].unit == unit_name:
            vocables.remove_at(i)
    unit_names.erase(unit_name)


func check_vocable_data():
    if not flow_units.get_children():
        button_file_open.texture_normal = button_green
        button_file_open.texture_hover = button_green_hovered
        button_file_open.texture_pressed = button_green_hovered
        button_unit_add.texture_normal = button_green
        button_unit_add.texture_hover = button_green_hovered
        button_unit_add.texture_pressed = button_green_hovered
        disable_button(button_file_save, true)
        label_warning.text = tr("Load a file or create a first unit.")
        hbox_warning.show()
    else:
        button_file_open.texture_normal = button_black
        button_file_open.texture_hover = button_black_hovered
        button_file_open.texture_pressed = button_black_hovered
        button_unit_add.texture_normal = button_black
        button_unit_add.texture_hover = button_black_hovered
        button_unit_add.texture_pressed = button_black_hovered
        if vocables.size() > 1:
            disable_button(button_file_save, false)
        else:
            disable_button(button_file_save, true)
        if tree_vocables.get_root().get_child_count() == 1:
            label_warning.text = tr("Type in some vocables for the current unit.")
            hbox_warning.show()
        else:
            hbox_warning.hide()


func disable_button(button, boolean):
    button.disabled = boolean
    if boolean:
        button.get_child(0).modulate = Color(1, 1, 1, 0.5)
    else:
        button.get_child(0).modulate = Color(1, 1, 1, 1)


func show_message_dialog(case):
    var text
    if case == "error":
        text = tr("Can't load vocabulary list!\nThe data structure is invalid.")
        message_dialog.label_title.text = tr("Error!")
        message_dialog.label_main.text = text
        message_dialog.button_close.hide()
    elif case == "new_file":
        text = tr("If you create a new vocabulary list all changes will be discarded!")
        message_dialog.label_title.text = tr("Warning!")
        message_dialog.label_main.text = text
        message_dialog.label_close.text = tr("Create without Saving") 
        message_dialog.button_close.show()
    elif case == "load_file":
        text = tr("If you open a new vocabulary list all changes will be discarded!")
        message_dialog.label_title.text = tr("Warning!")
        message_dialog.label_main.text = text
        message_dialog.label_close.text = tr("Open without Saving") 
        message_dialog.button_close.show()
    message_dialog.popup_centered()


func message_dialog_back_pressed():
    message_dialog.hide()


func message_dialog_close_pressed():
    message_dialog.hide()
    if message_dialog.label_close.text == tr("Create without Saving"):
        clear_data()
    elif message_dialog.label_close.text == tr("Open without Saving"):
        var title = tr("Select a File")
        var mode = DisplayServer.FILE_DIALOG_MODE_OPEN_FILE
        DisplayServer.file_dialog_show(title, global.default_dir, "", false, mode, csv_filter,
            _on_native_file_dialog_file_selected)
