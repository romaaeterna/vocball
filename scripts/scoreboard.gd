class_name Scoreboard
extends VBoxContainer


@onready var animation_scoreboard = $AnimationScoreboard
@onready var hbox_scoreboard = $HBoxScoreboard
@onready var label_score = hbox_scoreboard.get_node("MarginScore/HBoxScore/LabelScore")
@onready var label_team1 = hbox_scoreboard.get_node("LabelTeam1")
@onready var label_score1 = hbox_scoreboard.get_node("MarginScore/HBoxScore/LabelScore1")
@onready var label_team2 = hbox_scoreboard.get_node("LabelTeam2")
@onready var label_score2 = hbox_scoreboard.get_node("MarginScore/HBoxScore/LabelScore2")
@onready var label_main = $LabelMain
@onready var button_translation = $HBoxTranslation/ButtonTranslation
@onready var label_translation = $HBoxTranslation/LabelTranslation


func _on_button_translation_pressed():
    button_translation.hide()
    label_translation.show()
