class_name About
extends Panel


@onready var margin_credits = $MarginAbout/VBoxAbout/PanelCredits/MarginCredits
@onready var label_credits = margin_credits.get_node("LabelCredits")

@onready var animation_about = $AnimationAbout
@onready var game = get_tree().get_root().get_node("Game")
@onready var texture_blur = game.get_node("TextureBlur")


func _on_ready():
    label_credits.text = label_credits.text.format({
        "game": tr("Vocabulary Football").to_upper(),
        "copyright": tr("© 2024 Roma Aeterna"),
        "version": tr("Version"),
        "version_number": ProjectSettings.get_setting("application/config/version"),
        "programming": tr("Programming & Design").to_upper(),
        "snippets": tr("Code Snippets & Addons").to_upper(),
        "textures": tr("Textures & Icons").to_upper(),
        "fonts": tr("Fonts").to_upper(),
        "sounds": tr("Sounds").to_upper(),
        "licenses": tr("Licenses").to_upper(),
        "godot": tr("Made with Godot Engine 4")})


func _on_animation_about_animation_finished(animation_name):
    if animation_name == "slide_in":
        texture_blur.show()
    if animation_name == "slide_out":
        if global.keyboard_control:
            game.set_process(true)
        texture_blur.hide()


func _on_button_close_pressed():
    animation_about.play("slide_out")


func _on_label_credits_meta_clicked(meta):
    OS.shell_open(str(meta))
