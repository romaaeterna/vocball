class_name Global
extends Node


class vocable:
    var unit
    var source
    var target

    func _init(vocable_unit, vocable_source, vocable_target):
        self.unit = vocable_unit
        self.source = vocable_source
        self.target = vocable_target


class team:
    var number
    var name
    var country
    var score
    var texture
    var color_bg
    var color_fg

    func _init(team_number):
        self.number = team_number
        self.name = ""
        self.country = ""
        self.score = 0
        self.texture = null
        self.color_bg = null
        self.color_fg = null


# internal variables
var documents_dir = OS.get_system_dir(OS.SYSTEM_DIR_DOCUMENTS)
var in_game = false
var last_vocable_file = ""

# settings
## general
var countdown_length = 5
var keyboard_control = true
var language = OS.get_locale_language()
var sounds = 1.0
## vocables
var default_dir = documents_dir
var default_file = ""
var vocable_selection = "source"

# game setup
var player_number = 10
var team1 = team.new(1)
var team2 = team.new(2)
var unit_names = []
var unit_vocables = []
var vocables = []


func _init():
    randomize()
    load_settings()


func load_settings():
    var config = ConfigFile.new()
    config.load("user://settings.ini")

    countdown_length = config.get_value("General", "countdown", 5)
    keyboard_control = config.get_value("General", "keyboard_control", true)
    language = config.get_value("General", "language", OS.get_locale_language())
    sounds = config.get_value("General", "sounds", 1.0)
    default_dir = config.get_value("Vocables", "default_dir", documents_dir)
    default_file = config.get_value("Vocables", "default_file", "")
    vocable_selection = config.get_value("Vocables", "vocable_selection", "source")
    
    TranslationServer.set_locale(language)
    AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sounds"), linear_to_db(sounds))
