class_name Settings
extends Panel

var message_dialog = preload("res://scenes/message_dialog.tscn").instantiate()

var new_language = ""

@onready var vbox_settings = $MarginSettings/VBoxSettings
@onready var vbox_general = vbox_settings.get_node("PanelGeneral/MarginGeneral/VBoxGeneral")
@onready var hbox_language = vbox_general.get_node("GridGeneral/HBoxLanguage")
@onready var button_english = hbox_language.get_node("ButtonEnglish")
@onready var button_german = hbox_language.get_node("ButtonGerman")
@onready var button_latin = hbox_language.get_node("ButtonLatin")
@onready var hbox_sounds = vbox_general.get_node("GridGeneral/HBoxSounds")
@onready var slider_sounds = hbox_sounds.get_node("SliderSounds")
@onready var label_volume = hbox_sounds.get_node("LabelVolume")
@onready var hbox_timer = vbox_general.get_node("GridGeneral/HBoxTimer")
@onready var slider_timer = hbox_timer.get_node("SliderTimer")
@onready var label_length = hbox_timer.get_node("LabelLength")
@onready var check_keyboard = vbox_general.get_node("GridGeneral/CheckKeyboard")
@onready var vbox_vocables = vbox_settings.get_node("PanelVocables/MarginVocables/VBoxVocables")
@onready var grid_vocables = vbox_vocables.get_node("GridVocables")
@onready var button_default_dir = grid_vocables.get_node("HBoxDefaultDir/ButtonDefaultDir")
@onready var button_default_file = grid_vocables.get_node("HBoxDefaultFile/ButtonDefaultFile")
@onready var option_selection = grid_vocables.get_node("OptionSelection")

@onready var animation_settings = $AnimationSettings
@onready var game = get_tree().get_root().get_node("Game")
@onready var texture_blur = game.get_node("TextureBlur")


func _on_ready():
    add_child(message_dialog)
    message_dialog.button_back.pressed.connect(message_dialog_back_pressed)
    message_dialog.button_close.pressed.connect(message_dialog_close_pressed)
    apply_settings()


func _on_animation_settings_animation_finished(animation_name):
    if animation_name == "slide_in":
        texture_blur.show()
    if animation_name == "slide_out":
        if global.keyboard_control:
            game.set_process(true)
        texture_blur.hide()


func _on_button_close_pressed():
    animation_settings.play("slide_out")
    save_settings()


func _on_button_language_pressed(language_code):
    new_language = language_code
    if language_code != global.language:
        show_message_dialog()


func _on_slider_sounds_value_changed(value):
    global.sounds = value
    label_volume.text = "%s %%" % str(value * 100)
    AudioServer.set_bus_volume_db(AudioServer.get_bus_index("Sounds"), linear_to_db(value))


func _on_slider_timer_value_changed(value):
    global.countdown_length = value
    label_length.text = "%s s" % str(value)


func _on_check_keyboard_toggled(toggled):
    global.keyboard_control = toggled
    if global.keyboard_control:
        game.set_process(true)
    else:
        game.set_process(false)


func _on_button_default_dir_pressed():
    var dialog_title = tr("Select a Directory")
    var dialog_dir = global.default_dir if global.default_dir else global.documents_dir
    var dialog_mode = DisplayServer.FILE_DIALOG_MODE_OPEN_DIR
    var dialog_filter = PackedStringArray([])
    DisplayServer.file_dialog_show(dialog_title, dialog_dir, "", false, dialog_mode, dialog_filter,
        _on_native_file_dialog_dir_selected)


func _on_native_file_dialog_dir_selected(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    global.default_dir = selected_paths[0]
    button_default_dir.get_child(0).text = selected_paths[0].replace("\\", "/").split("/")[-1]


func _on_button_default_dir_revert_pressed():
    global.default_dir = ""
    button_default_dir.get_child(0).text = tr("None")


func _on_button_default_file_pressed():
    var dialog_title = tr("Select a File")
    var dialog_dir = global.default_dir if global.default_dir else global.documents_dir
    var dialog_mode = DisplayServer.FILE_DIALOG_MODE_OPEN_FILE
    var dialog_filter = PackedStringArray(["*.csv ; CSV Files"])
    DisplayServer.file_dialog_show(dialog_title, dialog_dir, "", false, dialog_mode, dialog_filter,
        _on_native_file_dialog_file_selected)


func _on_native_file_dialog_file_selected(_status, selected_paths, _selected_filter_index):
    if selected_paths.is_empty():
        return
    global.default_file = selected_paths[0]
    button_default_file.get_child(0).text = selected_paths[0].get_file()


func _on_button_default_file_revert_pressed():
    global.default_file = ""
    button_default_file.get_child(0).text = tr("None")


func _on_option_selection_item_selected(index):
    if index == 0:
        global.vocable_selection = "source"
    elif index == 1:
        global.vocable_selection = "target"
    elif index == 2:
        global.vocable_selection = "both"


func apply_settings():
    toggle_language_button()
    slider_sounds.value = global.sounds
    slider_timer.value = global.countdown_length
    check_keyboard.button_pressed = global.keyboard_control
    set_default_dir_and_file()
    set_vocable_selection_mode()


func toggle_language_button():
    if global.language == "de":
        button_german.button_pressed = true
    elif global.language == "en":
        button_english.button_pressed = true
    elif global.language == "ro":
        # "ro" = Latin language
        button_latin.button_pressed = true
    TranslationServer.set_locale(global.language)


func set_default_dir_and_file():
    if global.default_dir == "":
        button_default_dir.get_child(0).text = tr("None")
    else:
        var folder = global.default_dir.replace("\\", "/").split("/")[-1]
        button_default_dir.get_child(0).text = folder
    if global.default_file == "":
        button_default_file.get_child(0).text = tr("None")
    else:
        var file = global.default_file.get_file()
        button_default_file.get_child(0).text = file


func set_vocable_selection_mode():
    if global.vocable_selection == "source":
        option_selection.select(0)
    elif global.vocable_selection == "target":
        option_selection.select(1)
    elif global.vocable_selection == "both":
        option_selection.select(2)


func save_settings():
    var config = ConfigFile.new()
    config.set_value("General", "countdown", global.countdown_length)
    config.set_value("General", "keyboard_control", global.keyboard_control)
    config.set_value("General", "language", global.language)
    config.set_value("General", "sounds", global.sounds)
    config.set_value("Vocables", "default_dir", global.default_dir)
    config.set_value("Vocables", "default_file", global.default_file)
    config.set_value("Vocables", "vocable_selection", global.vocable_selection)
    config.save("user://settings.ini")


func show_message_dialog():
    message_dialog.label_title.text = tr("Restart Game?")
    message_dialog.label_main.text = tr("Do you want to restart the game to adapt the new language?")
    message_dialog.label_close.text = tr("Restart Game")
    message_dialog.popup_centered()


func message_dialog_back_pressed():
    message_dialog.hide()


func message_dialog_close_pressed():
    global.language = new_language
    TranslationServer.set_locale(new_language)
    save_settings()
    await get_tree().create_timer(0.1).timeout
    get_tree().reload_current_scene()
    game.reset_game()
