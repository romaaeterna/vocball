class_name Game
extends Control


var about = preload("res://scenes/about.tscn").instantiate()
var editor = preload("res://scenes/editor.tscn").instantiate()
var message_dialog = preload("res://scenes/message_dialog.tscn").instantiate()
var scoreboard = preload("res://scenes/scoreboard.tscn").instantiate()
var settings = preload("res://scenes/settings.tscn").instantiate()
var setup = preload("res://scenes/setup.tscn").instantiate()
var sound_goal = {"path": load("res://assets/sounds/goal.wav"), "db": -6}
var sound_kick_off = {"path": load("res://assets/sounds/kick_off.wav"), "db": -12}
var sound_lost_chance = {"path": load("res://assets/sounds/lost_chance.wav"), "db": -12}

var ball_position = 0
var current_countdown = global.countdown_length
var current_vocable = null
var current_vocables = []
var game_stage = "new_game"
var players = []
var res_player1_happy = ""
var res_player1_unhappy = ""
var res_player2_happy = ""
var res_player2_unhappy = ""


@onready var hbox_top = $TextureBackground/MarginTop/HBoxTop
@onready var button_next = hbox_top.get_node("ButtonNext")
@onready var label_next = button_next.get_node("LabelNext")
@onready var button_menu = hbox_top.get_node("ButtonMenu")
@onready var menu = hbox_top.get_node("ButtonMenu/Menu")

@onready var texture_scoreboard = $TextureBackground/MarginScoreboard/TextureScoreboard
@onready var margin_content = texture_scoreboard.get_node("MarginContent")

@onready var texture_pitch = $TextureBackground/MarginPitch/TexturePitch
@onready var animation_player = texture_pitch.get_node("AnimationPlayer")
@onready var texture_player1 = texture_pitch.get_node("TexturePlayer1")
@onready var texture_player2 = texture_pitch.get_node("TexturePlayer2")
@onready var label_player1 = texture_player1.get_node("MarginPlayer1/LabelPlayer1")
@onready var label_player2 = texture_player2.get_node("MarginPlayer2/LabelPlayer2")
@onready var texture_goal1 = texture_pitch.get_node("TextureGoal1/TextureColor1")
@onready var texture_goal2 = texture_pitch.get_node("TextureGoal2/TextureColor2")
@onready var texture_football = texture_pitch.get_node("TextureFootball")
@onready var hbox_buttons = texture_pitch.get_node("HBoxButtons")
@onready var button_left = hbox_buttons.get_node("ButtonLeft")
@onready var button_right = hbox_buttons.get_node("ButtonRight")
@onready var button_stay = hbox_buttons.get_node("ButtonStay")

@onready var texture_blur = $TextureBlur
@onready var audio_player = $AudioPlayer
@onready var animation_camera = $CameraScoreboard/AnimationCamera
@onready var timer_camera = $CameraScoreboard/TimerCamera
@onready var timer_countdown = $TimerCountdown


func _on_ready():
    set_process(global.keyboard_control)
    add_nodes()
    menu.get_popup().id_pressed.connect(_on_menu_item_pressed)


func _process(_delta):
    if Input.is_action_pressed("move_left") and hbox_buttons.is_visible():
        _on_button_movement_pressed(-1)
    if Input.is_action_pressed("move_right") and hbox_buttons.is_visible():
        _on_button_movement_pressed(1)
    if Input.is_action_pressed("next") and not button_next.disabled:
        _on_button_next_pressed()
    if Input.is_action_pressed("open_documentation"):
        _on_menu_item_pressed(4)
    if Input.is_action_pressed("open_editor"):
        _on_menu_item_pressed(3)
    if Input.is_action_pressed("open_settings"):
        _on_menu_item_pressed(2)
    if Input.is_action_pressed("quit"):
        _on_menu_item_pressed(6)


func _on_tree_exiting():
    about.queue_free()
    editor.message_dialog.queue_free()
    editor.queue_free()
    message_dialog.queue_free()
    scoreboard.queue_free()
    settings.queue_free()
    setup.message_dialog.queue_free()
    setup.queue_free()


func _on_button_next_pressed():
    if game_stage == "new_game":
        return stage_setup_game()
    if game_stage == "kick_off":
        return stage_kick_off()
    if game_stage == "player_selection":
        return stage_player_selection()
    if game_stage == "vocable_selection":
        return stage_vocable_selection()
    if game_stage == "goal_scored":
        return stage_goal_scored()


func _on_button_menu_pressed():
    menu.show_popup()


func _on_menu_item_pressed(id):
    if id == 0: # New game
        if global.in_game:
            show_message_dialog("new_game")
        else:
            stage_setup_game()
    if id == 1: # Game Setup
        stage_setup_game()
    if id == 2: # Settings
        set_process(false)
        settings.animation_settings.play("slide_in")
    if id == 3: # Editor
        set_process(false)
        editor.animation_editor.play("slide_in")
    if id == 4: # Documentation
        OS.shell_open("https://vocball.readthedocs.io/en/latest/")
    if id == 5: # About
        set_process(false)
        about.animation_about.play("slide_in")
    if id == 6: # Quit Game
        if global.in_game:
            show_message_dialog("quit")
        else:
            get_tree().quit()


func _on_button_movement_pressed(direction):
    game_stage = "player_selection"
    if direction == 0:
        hbox_buttons.hide()
        return
    move_ball(direction)


func _on_animation_camera_animation_started(animation_name):
    timer_camera.start()
    if animation_name == "zoom_in":
        scoreboard.animation_scoreboard.play("fade_out")
    if animation_name == "zoom_out":
        setup.animation_setup.play("fade_out")


func _on_animation_camera_animation_finished(animation_name):
    if animation_name == "zoom_out":
        button_menu.disabled = false
        button_next.disabled = false


func _on_timer_camera_timeout():
    if animation_camera.current_animation == "zoom_in":
        margin_content.remove_child(scoreboard)
        margin_content.add_child(setup)
        setup.animation_setup.play("fade_in")
    if animation_camera.current_animation == "zoom_out":
        margin_content.remove_child(setup)
        margin_content.add_child(scoreboard)
        scoreboard.animation_scoreboard.play("fade_in")
        setup_game()


func _on_animation_player_animation_started(_animation_name):
    button_menu.disabled = true
    button_next.disabled = true
    hbox_buttons.hide()


func _on_animation_player_animation_finished(animation_name):
    button_menu.disabled = false
    button_next.disabled = false
    
    if animation_name == "move":
        hbox_buttons.show()
    if animation_name == "goal":
        game_stage = "goal_scored"
        var text = tr("G O A L !")
        scoreboard.label_main.text = "[wave amp=60 freq=10][rainbow freq=0.25]%s[/rainbow][/wave]" % text
        scoreboard.button_translation.hide()
        scoreboard.label_translation.hide()
        if ball_position < -3:
            update_score_team1()
        if ball_position > 3:
            update_score_team2()
        if global.sounds:
            play_sound(sound_goal)


func _on_timer_countdown_timeout():
    current_countdown -= 1
    if current_countdown > 0:
        scoreboard.label_main.text = "%s …" % str(current_countdown)
        timer_countdown.start()
    else:
        button_menu.disabled = false
        button_next.disabled = false
        current_countdown = global.countdown_length
        timer_countdown.stop()
        stage_vocable_selection()


func add_nodes():
    add_child(about)
    add_child(editor)
    add_child(message_dialog)
    add_child(settings)
    margin_content.add_child(scoreboard)
    
    message_dialog.button_back.pressed.connect(message_dialog_back_pressed)
    message_dialog.button_close.pressed.connect(message_dialog_close_pressed)
    
    about.hide()
    editor.hide()
    settings.hide()
    setup.hide()


func reset_game():
    game_stage = "new_game"
    label_next.text = tr("New Game")
    menu.get_popup().set_item_disabled(1, true)
    setup.setup_finished = false
    global.in_game = false
    global.team1.score = 0
    global.team2.score = 0
    
    reset_scoreboard()
    reset_textures()


func setup_game():
    if not global.in_game and not setup.setup_finished:
        scoreboard.label_main.text = tr("Vocabulary Football")
        return
    if not global.in_game and setup.setup_finished:
        game_stage = "kick_off"
        label_next.text = tr("Kick-off!")
        menu.get_popup().set_item_disabled(1, false)
        players = []
        scoreboard.label_main.text = ""
        global.in_game = true
    
    setup_scoreboard()
    setup_textures()


func setup_scoreboard():
    current_vocables = global.unit_vocables.duplicate(true)
    scoreboard.label_score.text = ":"
    scoreboard.label_score1.text = str(global.team1.score)
    scoreboard.label_score2.text = str(global.team2.score)
    scoreboard.label_team1.add_theme_color_override("font_color", global.team1.color_fg)
    scoreboard.label_team1.get_theme_stylebox("normal").bg_color = global.team1.color_bg
    scoreboard.label_team1.text = global.team1.name
    scoreboard.label_team2.add_theme_color_override("font_color", global.team2.color_fg)
    scoreboard.label_team2.get_theme_stylebox("normal").bg_color = global.team2.color_bg
    scoreboard.label_team2.text = global.team2.name


func setup_textures(): 
    label_player1.add_theme_color_override("font_color",global.team1.color_fg)
    label_player2.add_theme_color_override("font_color", global.team2.color_fg)
    res_player1_happy = "res://assets/textures/player_%s_happy.svg" % global.team1.country
    res_player1_unhappy = "res://assets/textures/player_%s_unhappy.svg" % global.team1.country
    res_player2_happy = "res://assets/textures/player_%s_happy.svg" % global.team2.country
    res_player2_unhappy = "res://assets/textures/player_%s_unhappy.svg" % global.team2.country
    texture_player1.texture = load(res_player1_happy)
    texture_player2.texture = load(res_player2_happy)
    texture_goal1.modulate = global.team1.color_bg
    texture_goal1.show()
    texture_goal2.modulate = global.team2.color_bg
    texture_goal2.show()


func reset_scoreboard():
    scoreboard.label_main.text = ""
    scoreboard.label_score.text = ""
    scoreboard.label_score1.text = ""
    scoreboard.label_score2.text = ""
    scoreboard.label_team1.text = ""
    scoreboard.label_team2.text = ""
    scoreboard.label_team1.get_theme_stylebox("normal").bg_color = Color.TRANSPARENT
    scoreboard.label_team2.get_theme_stylebox("normal").bg_color = Color.TRANSPARENT
    scoreboard.label_translation.hide()
    scoreboard.button_translation.hide()


func reset_textures():
    res_player1_happy = ""
    res_player2_happy = ""
    texture_goal1.hide()
    texture_goal2.hide()
    hbox_buttons.hide()
    move_back_to_center()


func stage_setup_game():
    button_menu.disabled = true
    button_next.disabled = true
    animation_camera.play("zoom_in")


func stage_kick_off():
    game_stage = "player_selection"
    stage_player_selection()
    if global.sounds:
        play_sound(sound_kick_off)


func stage_player_selection():
    game_stage = "vocable_selection"
    var player_number = get_player_number()
    label_next.text = tr("Next")
    label_player1.text = str(player_number)
    label_player2.text = str(player_number)
    hbox_buttons.hide()
    scoreboard.button_translation.hide()
    scoreboard.label_translation.hide()
    start_countdown()


func stage_vocable_selection():
    game_stage = "player_selection"
    get_vocable()
    hbox_buttons.show()
    scoreboard.button_translation.show()


func stage_goal_scored():
    game_stage = "kick_off"
    move_back_to_center()
    label_next.text = tr("Kick-off!")
    scoreboard.label_main.text = ""


func start_countdown():
    button_menu.disabled = true
    button_next.disabled = true
    current_countdown = global.countdown_length
    scoreboard.label_main.text = "%s …" % current_countdown
    timer_countdown.start()


func get_player_number():
    if players == []:
        players = range(1, global.player_number + 1)
    var player = players[randi_range(0, len(players) - 1)]
    players.erase(player)
    return player


func get_vocable():
    current_vocable = current_vocables.pick_random()
    current_vocables.erase(current_vocable)
    if current_vocables.is_empty():
        current_vocables = global.unit_vocables.duplicate(true)
    
    var random_selection = ""
    if global.vocable_selection == "both":
        random_selection = ["source", "target"].pick_random()
    if global.vocable_selection == "source" or random_selection == "source":
        scoreboard.label_main.text = current_vocable.source
        scoreboard.label_translation.text = current_vocable.target
    if global.vocable_selection == "target" or random_selection == "target":
        scoreboard.label_main.text = current_vocable.target
        scoreboard.label_translation.text = current_vocable.source


func move_ball(direction):
    ball_position += direction
    if ball_position < -3:
        return goal_animation_team2()
    if ball_position > 3:
        return goal_animation_team1()
    if global.sounds:
        if (ball_position == -2 and direction == 1) or (ball_position == 2 and direction == -1):
            play_sound(sound_lost_chance)
    animate_ball_movement(direction)


func animate_ball_movement(direction):
    var animation = animation_player.get_animation("move")
    var movement_x = 225 * direction
    var random_direction = [-1, 1]
    if texture_player1.position.y < 85:
        random_direction = [1]
    if hbox_buttons.position.y > 575:
        random_direction = [-1]
    var movement_y = randi_range(15, 85) * random_direction.pick_random()
    
    var index = 0
    for node in get_tree().get_nodes_in_group("player_nodes"):
        animation.track_insert_key(index, 0, Vector2(node.position.x, node.position.y))
        animation.track_insert_key(index, 1, Vector2(node.position.x + movement_x,
            node.position.y + movement_y))
        index += 1
    animation_player.play("move")


func goal_animation_team1():
    var animation = animation_player.get_animation("goal")
    animation.track_insert_key(0, 0, Vector2(texture_football.position))
    animation.track_insert_key(0, 0.5, Vector2(texture_football.position))
    animation.track_insert_key(0, 2, Vector2(1788, 315))
    animation.track_insert_key(1, 0, Vector2(1, 1))
    animation.track_insert_key(1, 0.5, Vector2(1, 1))
    animation.track_insert_key(1, 2, Vector2(0.5, 0.5))
    animation.track_insert_key(2, 0, texture_player1.position.y)
    animation.track_insert_key(2, 1, texture_player1.position.y)
    animation.track_insert_key(2, 2, texture_player1.position.y)
    animation.track_insert_key(3, 0, texture_player2.position.y)
    animation.track_insert_key(3, 1, texture_player2.position.y - 150)
    animation.track_insert_key(3, 2, texture_player2.position.y)
    animation_player.play("goal")


func goal_animation_team2():
    var animation = animation_player.get_animation("goal")
    animation.track_insert_key(0, 0, Vector2(texture_football.position))
    animation.track_insert_key(0, 0.5, Vector2(texture_football.position))
    animation.track_insert_key(0, 2, Vector2(50, 315))
    animation.track_insert_key(1, 0, Vector2(1, 1))
    animation.track_insert_key(1, 0.5, Vector2(1, 1))
    animation.track_insert_key(1, 2, Vector2(0.5, 0.5))
    animation.track_insert_key(2, 0, texture_player1.position.y)
    animation.track_insert_key(2, 1, texture_player1.position.y - 150)
    animation.track_insert_key(2, 2, texture_player1.position.y)
    animation.track_insert_key(3, 0, texture_player2.position.y)
    animation.track_insert_key(3, 1, texture_player2.position.y)
    animation.track_insert_key(3, 2, texture_player2.position.y)
    animation_player.play("goal")


func update_score_team1():
    global.team1.score += 1
    if global.team1.score > 9:
        global.team1.score = 0
    scoreboard.label_score1.text = str(global.team1.score)
    setup.slider_goal1.value = global.team1.score
    setup.label_goal1.text = str(global.team1.score)
    texture_player2.texture = load(res_player2_unhappy)


func update_score_team2():
    global.team2.score += 1
    if global.team2.score > 9:
        global.team2.score = 0
    scoreboard.label_score2.text = str(global.team2.score)
    setup.slider_goal2.value = global.team2.score
    setup.label_goal2.text = str(global.team2.score)
    texture_player1.texture = load(res_player1_unhappy)


func move_back_to_center():
    ball_position = 0
    label_player1.text = ""
    label_player2.text = ""
    texture_player1.texture = load(res_player1_happy) if res_player1_happy else null
    texture_player2.texture = load(res_player2_happy) if res_player2_happy else null
    texture_player1.position = Vector2(1004, 175)
    texture_player2.position = Vector2(775, 175)
    texture_football.position = Vector2(894, 315)
    texture_football.scale = Vector2(1, 1)
    hbox_buttons.position = Vector2(798, 435)


func play_sound(sound_data):
    if audio_player.is_playing():
        audio_player.stop()
    audio_player.stream = sound_data["path"]
    audio_player.volume_db = sound_data["db"]
    audio_player.play()


func show_message_dialog(case):
    if case == "new_game":
        message_dialog.label_title.text = tr("Abort Game?")
        message_dialog.label_main.text = tr("Do you want to start a new game?")
        message_dialog.label_close.text = tr("New Game")
    if case == "quit":
        message_dialog.label_title.text = tr("Quit Game?")
        message_dialog.label_main.text = tr("Do you want to quit the application?")
        message_dialog.label_close.text = tr("Quit Game")
    message_dialog.popup_centered()


func message_dialog_back_pressed():
    message_dialog.hide()


func message_dialog_close_pressed():
    message_dialog.hide()
    if message_dialog.label_close.text == tr("New Game"):
        reset_game()
        stage_setup_game()
    elif message_dialog.label_close.text == tr("Quit Game"):
        get_tree().quit()
