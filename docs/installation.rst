Installation
============


Microsoft Windows
-----------------

To install Vocabulary Football follow the setup wizard of `VocBallSetup.exe <https://romaaeterna.itch.io/vocball>`_.

If you don't want to install the game, just unzip `VocBallWindows.zip <https://romaaeterna.itch.io/vocball>`_ and execute the file ``VocBall.exe``.


Linux
-----

To play Vocabulary Football just unzip `VocBallLinux.zip <https://romaaeterna.itch.io/vocball>`_. and execute the file ``VocBall.sh``.


Build with Godot Engine
-----------------------

If you want to build the game on your own, modify or improve it, feel free to do so.
All you need is the `Godot Engine <https://godotengine.org/>`_ (>= 4.3) and the `source code hosted on GitLab <https://gitlab.com/romaaeterna/vocball>`_.
