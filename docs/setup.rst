Getting started
===============


Vocabulary list
---------------

To play Vocabulary Football you need a vocabulary list.
You can use the build-in vocabulary editor, which features easy and intuitive operation, to create new lists or edit existing ones.

.. image:: images/screen_editor.png

Alternatively, you can create and edit vocabulary lists as CSV files by your own.
Please note that the application can only load files with the following data structure::

   [unit],[vocable],[translation]

Your vocabulary list should look like this example (separated by comma, semicolon or tab)::

   1,this,dies
   1,is,ist
   2,an,ein
   2,example,Beispiel


Teams and player numbers
------------------------

Like real football Vocabulary Football is played between two teams.
Both teams should be equally big and consist of 4 to 16 players.
Each player of a team has got a number (like on a football jersey).
The players with the same number compete against each other.
When every student is assigned to his/her team and number, you can start the game.


Settings
--------

There are several general settings you may adapt to your needs.

.. image:: images/screen_settings.png
