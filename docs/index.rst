Vocabulary Football (VocBall)
=============================

.. image:: images/logo.png
    :align: center

****

**VocBall is a simple application to play vocabulary football in class.**

The program is released under the `GNU General Public License (GPL) version 3 <http://www.gnu.org/licenses/>`_.


Contents
--------

.. toctree::
   :maxdepth: 2

   installation
   setup
   game
   development
   credits
