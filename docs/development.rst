Development
===========

The current version of Vocabulary Football is 2.0.0.


Bugs
----

Known Bugs
~~~~~~~~~~

At the moment no bugs are known.

Filing A Bug
~~~~~~~~~~~~

If you’ve found a bug in Vocabulary Football, please head over to GitLab and `file a report <https://gitlab.com/romaaeterna/vocball/issues>`_.
Filing bugs helps improve the software for everyone.


Source Code
-----------

The full source code of Vocabulary Football is hosted on `GitLab <https://gitlab.com/romaaeterna/vocball>`_.
