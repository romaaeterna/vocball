How to play
===========


Setting up the game
-------------------

After starting the game you'll see the main window of the application.
Click the button "New Game" to open the setup screen.

.. image:: images/screen_main.png

First click on the button at the top left and navigate to the file you want to open.
Now you can choose the units you want your students to repeat.
Furthermore you can select the kind of vocable selection and number of players each team consists of.

.. image:: images/screen_setup_1.png

Next you need to go to the "Teams" tab.
Type in the name of the teams. If you like, you can change the jerseys and number of scored goals.
Finally press the button "Apply".

.. image:: images/screen_setup_2.png


Playing the game
----------------

You'll find the team names and the current scores on the top of the scoreboard.
Click on the button "Kick-off!" to start the game.

.. image:: images/screen_game_1.png

Now a countdown starts which you'll see in the middle of the scoreboard.
At the same time the current player number will appear on two jerseys near the ball on the football field.

.. image:: images/screen_game_2.png

When the countdown is finished, a random vocable will be shown in the middle of the scoreboard.
It has to be translated by the students who have got the current player number.

.. image:: images/screen_game_3.png

After a correct answer move the football towards the opponents goal by clicking on the appropriate arrow (or by pressing the left or right arrow key).
After a wrong answer move the ball to the other direction.
You can show the answer by clicking on the appropriate button.

.. image:: images/screen_game_4.png

When the football reaches the left or the right side of the screen, a goal is scored.
Continue the game by clicking on the button "Kick-off!" (or pressing the enter key).

.. image:: images/screen_game_5.png
